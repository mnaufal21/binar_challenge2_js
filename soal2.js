const chectTypeNumber = data => {
    if (!data){
        return 'Error: do not input'
    } else if(typeof(data) === 'number'){
        if (data % 2 === 0) {
            return 'Genap'
        } else{
            return 'Ganjil'
        }
    } else {
        return 'Error: input data invalid'
    }
}

console.log(chectTypeNumber(10))
console.log(chectTypeNumber(3))
console.log(chectTypeNumber('3'))
console.log(chectTypeNumber({}))
console.log(chectTypeNumber([]))
console.log(chectTypeNumber())