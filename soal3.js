function checkEmail(email) {
    const regex = /\S+@\S+\.\S+/
    if (!email) {
        return 'Error tidak ada input di parameter'
    } else if (!regex) {
        return 'Error: Format email salah'
    } else if (regex.test(email) === true) {
        return 'Valid'
    } else if (regex.test(email) === false) {
        return 'Invalid'
    }
}

console.log(checkEmail('apranata@binar.co.id'))
console.log(checkEmail('apranata@binar.com'))
console.log(checkEmail('apranata@binar'))
console.log(checkEmail('apranata'))
console.log(checkEmail(3322))
console.log(checkEmail())