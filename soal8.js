const dataPenjualanNovel = [
    {
        idProduct: 'BOOK002421',
        namaProduk: 'Pulang - Pergi',
        penulis: 'Tere Liye',
        hargaBeli: 60000,
        hargaJual: 86000,
        totalTerjual: 150,
        sisaStok: 17
    },
    {
        idProduct: 'BOOK002351',
        namaProduk: 'Selamat Tinggal',
        penulis: 'Tere Liye',
        hargaBeli: 75000,
        hargaJual: 103000,
        totalTerjual: 171,
        sisaStok: 20
    },
    {
        idProduct: 'BOOK002941',
        namaProduk: 'Garis Waktu',
        penulis: 'Fiersa Besari',
        hargaBeli: 67000,
        hargaJual: 99000,
        totalTerjual: 213,
        sisaStok: 5
    },
    {
        idProduct: 'BOOK002961',
        namaProduk: 'Laskar Pelangi',
        penulis: 'Andrea Hirata',
        hargaBeli: 55000,
        hargaJual: 68000,
        totalTerjual: 20,
        sisaStok: 56
    }
]

const getInfoPenjualan = dataPenjualan => {
    let totalKeuntungan = null
    let totalModal = null
    for (let i = 0; i < dataPenjualan.length; i++) {
        let untung = (dataPenjualan[i].hargaJual - dataPenjualan[i].hargaBeli) * dataPenjualan[i].totalTerjual
        totalKeuntungan += untung
        let modal = (dataPenjualan[i].totalTerjual + dataPenjualan[i].sisaStok) * dataPenjualan[i].hargaBeli
        totalModal += modal
    }

    function printMoney(number) {
        let reverse = number.toString().split('').reverse().join(''),
            ribuan = reverse.match(/\d{1,3}/g);
        ribuan = ribuan.join('.').split('').reverse().join('');
        return ribuan
    }

    const persentaseKeuntungan = (totalKeuntungan / totalModal) * 100

    const orderedData = dataPenjualan.map((data) => data.totalTerjual)
    // const orderedData = new Array(dataPenjualan.sort((elem1, elem2) => { return elem1.totalTerjual - elem2.totalTerjual}))
    const maxSales = Math.max.apply(null, orderedData)
    const bestSeller = dataPenjualan.filter(book => book.totalTerjual === maxSales)

    const bestAuthor = Array.from(dataPenjualan.reduce((m, { penulis, totalTerjual }) => m.set(penulis, (m.get(penulis) || 0) + totalTerjual), new Map), ([penulis, totalTerjual]) => ({ penulis, totalTerjual }))

    return {
        totalKeuntungan: `Rp.${printMoney(totalKeuntungan)}`,
        totalModal: `Rp.${printMoney(totalModal)}`,
        persentaseKeuntungan: `${persentaseKeuntungan.toFixed(2)}%`,
        bukuTerlaris: bestSeller[0].namaProduk,
        penulisTerlaris: bestAuthor[0].penulis
    }
}

console.log(getInfoPenjualan(dataPenjualanNovel))